// Text header of the page, reminding the current section
#let header(palette: none, title: none) = (
  context {
    let current_page = here().page()
    if current_page > 1 {
      let elems = query(
        heading.where(level: 1, outlined: true).before(here()),
      )

      let appendix = query(<appendix_start>)
      let in_appendix = appendix != () and current_page > (
        appendix.first().location().page()
      )

      set text(fill: palette.darker)
      show text: smallcaps

      if elems == () {
        title
      } else {
        elems.last().body
      }
      if in_appendix {
        [ --- Appendix]
      }
    }
  }
)
// Background shapes, including the page counter
#let background(
  margins: (:),
  palette: (:),
  single-column: none,
) = (
  context if here().page() > 1 {
    if single-column {
      place(
        right + top,
        block(
          width: margins.x,
          height: margins.top * 80%,
          align(
            horizon + center,
            text(size: 28pt, fill: palette.lighter, counter(page).display()),
          ),
        ),
      )
    } else {
      place(
        left + top,
        rect(fill: palette.lightest, width: 100%, height: margins.top * 80%),
      )

      place(
        right + top,
        block(
          label,
          fill: palette.lighter,
          width: margins.x,
          height: margins.top * 80%,
          align(
            horizon + center,
            text(size: 28pt, fill: palette.lightest, counter(page).display()),
          ),
        ),
      )
    }
  }
)


// Text footer, with various info, displayed at specific locations.
#let footer(site: none) = (
  context {
    let current_page = here().page()
    let appendix = query(<appendix_start>)
    let body_last = appendix != () and current_page == (
      appendix.first().location().page()
    )
    let last = current_page == counter(page).final().at(0)
    if last or body_last {
      let this_year = datetime.today().display("[month repr:long] [year]")
      align(
        right,
        {
          link(site)[Made with Typst]
          box(inset: (x: 0.3em), scale(x: -100%, sym.copyright))
          this_year
        },
      )
    }
  }
)

// Configure headings. Numberings are expected to be dot-separated.
#let heading_style(palette: (:), it) = (
  context {
    // Find out the final number of the heading counter.
    let levels = counter(heading).get()
    let deepest = if levels != () {
      levels.last()
    } else {
      1
    }

    set text(10pt, weight: 400)

    if it.level == 1 {
      set text(weight: 600, size: 1em + 2pt)

      v(20pt, weak: true)
      if it.numbering != none {
        let number_styl = it.numbering.split(".").at(0)
        box(fill: palette.default, outset: 4pt, numbering(number_styl, deepest))
        h(7pt, weak: true)
      }
      it.body
      v(13.75pt, weak: true)
    } else if it.level == 2 {
      set text(weight: 600)

      v(15pt, weak: true)
      if it.numbering != none {
        let number_styl = it.numbering.split(".").at(1)
        box(fill: palette.lighter, outset: 3pt, numbering(number_styl, deepest))
        h(7pt, weak: true)
      }
      it.body
      v(10pt, weak: true)
    } else if it.level == 3 {
      v(0pt)
      emph(text(fill: palette.darker, it.body + [. ]))
    } else {
      let page = context here().page()
      panic("Heading at page " + str(page) + " has level " + str(it.level) + ", while 3 is the maximum.")
    }
  }
)
