// Simple functions for boxed theorems environments.
// This uses figures so as to be able to reference them in a straightforward way
// (as opposed to solutions like typst-theorems using custom reference functions)

// So far, these functions have to be used by the template itself so that it can generate the corresponding
// environment functions. To circumvent this problem and use the given functions directly from the document,
// it should be possible to include labels in the format function, next to a box for instance so that the theme
// may be able to change its colour etc. Label functionalities are somewhat still limited at the moment though.

// Another problem is that it is hard to provide directly a simple interface to users while using figures.
// Figures need to be styled using the `show` directive. This is neat, but it means the user must have at
// least two functions to define its own environments (one for the show directive, the user to define the
// figure wrapper; these can't be joined together, as the resulting content is seen from the outside as a
// `styled`, and not a `figure`, which prevents simple referencing).

#let default_fmt(
  background: black.lighten(90%),
  foreground: black.lighten(50%),
  textcolour: black,
  supplement,
  number,
  name,
  body,
) = block(
  fill: background,
  radius: 5pt,
  inset: 8pt,
  {
    box(fill: foreground, radius: 3pt, outset: 0.4em)[#supplement #number]
    h(0.6em)
    if name != none {
      text(fill: textcolour, emph(name + [. ]))
    }
    body
  },
)


// A theorem environment with a given formatter, identifier and fullname. This is used by the template
// directly (see the comments above).
// Please note that this cannot be labeled directly from outside, as it appears as a `styled(...)` and
// note a figure, hence the extra `label` argument.
#let thmenv(fmt: default_fmt, identifier, fullname) = {
  let label_type = label

  (name: none, label: none, body) => {
    show figure.where(kind: identifier): it => fmt(
      it.supplement,
      it.counter.display(it.numbering),
      it.caption,
      it.body,
    )
    [
      #figure(
        caption: name,
        supplement: fullname,
        kind: identifier,
        outlined: false,
        body,
      )
      #if type(label) == str {
        label_type(label)
      }
    ]
  }
}

#let thmenv_array(fmt: default_fmt, fmt_emph: default_fmt, arr) = {
  let thmenv_switch(identifier: none, fullname: none, emph: false) = {
    if type(identifier) != str or type(fullname) != str {
      panic("theorems: `identifier` and `fullname` should be strings")
    }
    if emph {
      thmenv(fmt: fmt_emph, identifier, fullname)
    } else {
      thmenv(fmt: fmt, identifier, fullname)
    }
  }

  let dict = (:)
  for args in arr {
    let env = thmenv_switch(..args)
    dict.insert(args.at("fullname"), env)
  }
  return dict
}
