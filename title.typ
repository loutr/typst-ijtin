#let maketitle(font, title, authors) = {
  // Display the paper's title.
  align(center, text(18pt, title))
  v(8.35mm, weak: true)

  // Display the authors list.
  for i in range(calc.ceil(authors.len() / 3)) {
    let end = calc.min((i + 1) * 3, authors.len())
    let is-last = authors.len() == end
    let slice = authors.slice(i * 3, end)
    grid(
      columns: slice.len() * (1fr,),
      gutter: 12pt,

      ..slice.map(author => align(
        center + top,
        {
          author.at("prefix", default: none) + [ ]
          text(12pt, author.name)

          if "email" in author {
            linebreak()
            text(
              font: font,
              size: 9pt,
              link("mailto:" + author.email),
            )
          }
          if "organization" in author {
            linebreak()
            emph(author.organization)
          }
        },
      ))
    )

    if not is-last {
      v(16pt, weak: true)
    }
  }
  v(35pt, weak: true)
}

