// I just think it's neat
// Article template. use the `article` function as a global `show` rule.


// The article palette.
#let palette(
  accent: rgb("#15c269"),
) = (
  default: accent,
  darker: accent.darken(66%), // links, accents
  lighter: accent.lighten(45%), // header bar (page count)
  lightest: accent.lighten(77%), // header bar, background for text
  darker_op: accent.negate().darken(66%),
  lighter_op: accent.negate().lighten(45%),
  lightest_op: accent.negate().lighten(77%),
)

// Generates boxed environment from a given accent colour.
#let boxed(
  accent: rgb("#15c269"),

  // List of dictionaries describing the desired box environments.
  boxenv: (
    (identifier: "definition", fullname: "Definition"),
    (identifier: "theorem", fullname: "Theorem", emph: true),
    (identifier: "lemma", fullname: "Lemma")
  ),
) = {
  // Theorem formatting functions
  import "theorems.typ": default_fmt, thmenv_array

  let palette = palette(accent: accent)

  let fmt = default_fmt.with(
    background: palette.lightest,
    foreground: palette.lighter,
    textcolour: palette.darker,
  )
  let fmt_emph = default_fmt.with(
    background: palette.lightest_op,
    foreground: palette.lighter_op,
    textcolour: palette.darker_op,
  )

  thmenv_array(fmt: fmt, fmt_emph: fmt_emph, boxenv)
}


#let article(
  // The paper's title.
  title: "Paper Title",

  // The hosting repository of this article, if any.
  // This is where the footer link points to.
  site: "https://github.com/typst/typst",

  // The font used in the document. Either a string or a dictionary that can
  // contain the keys "body", "mono", "math".
  font: "Linux Libertine",

  // single-column mode, with wider margins, bigger text and a different banner
  single-column: false,

  // An array of authors. For each author you can specify a name,
  // an organization, and an email. Everything but the name is optional.
  authors: (),

  // The paper's abstract. Can be omitted if you don't have one.
  abstract: none,

  // The (absolute) path to a bibliography file if you want to
  // cite some external works.
  bibliography-file: none,

  // The default accent colour of the paper.
  accent: rgb("#15c269"),

  // The appendix of the article.
  appendix: none,
) = {
  // Create a simple colour palette from the accent colour.
  if type(font) != dictionary {
    font = (
      body: font,
    )
  }

  // Build a font list, if not already provided.
  font = (
    body: font.at("body"),
    mono: font.at("mono", default: font.at("body") + " Mono"),
    math: font.at("math", default: "New Computer Modern Math"),
  )

  let palette = palette(accent: accent)

  body => {
    // The article
    // Set document metdata.
    set document(title: title, author: authors.map(author => author.name))

    // Set the fonts.
    set text(
      font: font.body,
      size: if single-column {
        11pt
      } else {
        10pt
      },
    )
    show math.equation: set text(font: font.math)

    // Configure equation numbering and spacing.
    set math.equation(numbering: "(1)")
    show math.equation: set block(spacing: 0.65em)

    // Configure lists.
    set enum(indent: 10pt, body-indent: 9pt)
    set list(indent: 10pt, body-indent: 9pt)

    // Configure links and citations
    show link: set text(fill: palette.darker)
    show cite: set text(fill: palette.default)

    // The margins.
    let margins = if single-column {
      (x: 2.5cm, top: 3.4cm, bottom: 2.5cm)
    } else {
      (x: 41.5pt, top: 80.51pt, bottom: 89.51pt)
    }
    let margins_first_page = auto

    import "styles.typ": header, background, footer, heading_style

    // Configure the page.
    set page(
      margin: margins_first_page,
      header: header(title: title, palette: palette),
      background: background(
        margins: margins,
        single-column: single-column,
        palette: palette,
      ),
      footer: footer(site: site),
    )
    show heading: heading_style.with(palette: palette)

    // Show the title.
    import "title.typ": maketitle
    maketitle(font.mono, title, authors)

    set par(justify: true)

    // Abstract and outlines are delicately padded.
    pad(
      x: 10%,
      {
        // Display abstract
        if abstract != none {
          block(outset: 10pt, radius: 8pt, width: 100%, fill: palette.lightest)[
            _Abstract_ --- #abstract
            #v(2pt)
          ]
        }

        // Content outline
        outline(
          indent: auto,
          depth: 2,
          target: selector(heading).before(<appendix_start>),
        )

        // Appendix outline, if any
        if appendix != none {
          outline(
            indent: auto,
            title: "Appendices",
            depth: 1,
            target: selector(heading).after(
              selector(<appendix_start>),
              inclusive: false,
            ),
          )
        }
      },
    )

    // Main content formatting
    set page(margin: margins)
    pagebreak()
    show: columns.with(
      if single-column {
        1
      } else {
        2
      },
      gutter: 12pt,
    )

    // Choose numbering
    set heading(numbering: "I.1.i")

    // Display the paper's contents
    body

    // Display bibliography
    if bibliography-file != none {
      show bibliography: set text(9pt)
      bibliography(bibliography-file, title: "References", style: "ieee")
    }

    if appendix != none {
      // Change numbering style and reset counter
      set heading(numbering: "A.a.i", supplement: "Appendix")
      counter(heading).update(0)

      v(20pt)
      text(size: 15pt, strong[Appendices <appendix_start>])
      appendix
    }
  }
}
